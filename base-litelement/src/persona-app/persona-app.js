import {html, LitElement} from 'lit-element';
import { render } from 'lit-html';
import '../persona-header/persona-header'
import '../persona-main/persona-main'
import '../persona-footer/persona-footer'
import '../persona-sidebar/persona-sidebar'
import '../persona-stats/persona-stats'


class PersonaApp extends LitElement {

    static get properties() {
        return {
           // propName: { type: String }
           people: { type: Array}
        };
    }
    constructor() {
        super();
        this.people = []

    }
    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar class="col-2" @new-person="${this.newPerson}"></persona-sidebar>
		        <persona-main class="col-10" @updated-people= "${this.updatePeople}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.peopleStatsUpdated}"></persona-stats>
            `;

    }
    peopleStatsUpdated(e){
        console.log("Peoplestatsupdated .. ");
        this.shadowRoot.querySelector("persona-sidebar").peopleStats = e.detail.peopleStats;


    }
    newPerson(e) {
        console.log("newPerson en Persona-App");	
        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }
    updatePeople(e) {
        console.log("updatedPeople");
        console.log(e.detail.people)
        this.people = e.detail.people;
    }
    updated(changedProperties){
        console.log("updated en personaapp");

        if(changedProperties.has("people")) {

         console.log("Ha cambiado el valor de la propiedad")
         this.shadowRoot.querySelector("persona-stats").people = this.people;

        }

        }

    

    }
    //final


customElements.define('persona-app', PersonaApp);

