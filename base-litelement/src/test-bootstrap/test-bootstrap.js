import { LitElement, html, css } from 'lit-element';
class TestBootstrap extends LitElement {

    static get styles() {
        return css`
            .redbg { 
                background-color: red;
            }
            .greenbg {
                background-color:green
            }
            .bluebg{
                background-color: blue
            }
    `}
    render() {
        return html`
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <h3>Test Bootstrap</h3>
            <div class="row greybg">
                <div class="col offset-1 redbg">Col 1</div>
                <div class="col offset-1 greenbg">Col 2</div>
                <div class="col offset-1 bluebg">Col 3</div>
            </div>
        `;
    }
}
customElements.define('test-bootstrap', TestBootstrap)