import { LitElement, html} from 'lit-element';
import '../emisor-evento/emisor-evento.js'
import '../receptor-evento/receptor-evento.js'

 

class GestorEvento extends LitElement {

    render() {

        return html`

            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}" ></emisor-evento>
            <receptor-evento id="el-que-recibe"></receptor-evento>
        `;

    }

    processEvent(e) {
        console.log("Capturado el evento del emisor");
        console.log(e.detail);
// lo que esta en la izquierda es la propiedad del receptor, en la derecha la propiedad del emisor
        this.shadowRoot.getElementById("el-que-recibe").course = e.detail.course;
        this.shadowRoot.getElementById("el-que-recibe").year = e.detail.year;
    }

}

 

customElements.define('gestor-evento', GestorEvento)