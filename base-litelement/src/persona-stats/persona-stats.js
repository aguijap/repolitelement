import {html, LitElement} from 'lit-element';
import { render } from 'lit-html';

class PersonaStats extends LitElement {

    static get properties(){
        return {
            people: {type: Array}
        }
    }
    constructor(){
        super();

        this.people =[];
    }



updated(changedProperties){
    console.log("updated en personaapp");

    if(changedProperties.has("people")) {

     console.log("Ha cambiado el valor de la propiedad")

    let peopleStats = this.gatherPeopleArrayInfo(this.people)

    this.dispatchEvent(
        new CustomEvent("updated-people-stats", {
            detail: {
                peopleStats: peopleStats
            }
        })
    )

    }



}
gatherPeopleArrayInfo(people){
    console.log(" GatherPeopleStat")
    let peopleStats = {}
    peopleStats.numberOfPeople = people.length;

    return peopleStats;

}
}
customElements.define('persona-stats', PersonaStats);

