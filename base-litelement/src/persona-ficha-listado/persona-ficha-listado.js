import { html, LitElement } from 'lit-element';
import { render } from 'lit-html';

class PersonaFichaListado extends LitElement {

    static get properties() {
        return {
            fname: { type: String },
            yearsInCompany: { type: String },
            photo: { type: Object },
            profile: {type: String},
            personInfo: { type: String }

        };
    }
    constructor() {
        super();


    }
    render() {
        return html`
           
           <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous"> 
           <div class="card h-100">
                <img src="${this.photo.src}" alt="${this.photo.alt}" height="100" width="100">    
                <div class="card-body">
                    <h5 class="card-tittle">${this.fname}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer">

                    <button @click="${this.deletePerson}" class="btn btn-danger col-5">X</button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1">Info</button>
                </div>
           </div>
            `;
    }
    moreInfo(e){
        console.log("moreInfo");
        console.log("Se ha pedido mas info de la persona: " + this.fname);

        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name: this.fname
                }
        }))
    }

    deletePerson(e) {
        console.log("deletePerson");
        console.log("se va a borrar la persona de nombre " + this.fname)
        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    name: this.fname
                }
            })
        )
        }

    }


customElements.define('persona-ficha-listado', PersonaFichaListado);

