import {html, LitElement} from 'lit-element';
import { render } from 'lit-html';

class PersonaHeader extends LitElement {

    static get properties() {
        return {
            propName: { type: String }
        };
    }
    constructor() {
        super();
        
    }
    render() {
        return html`
            <h1> Persona Header</h1>
            `;

    }
}

customElements.define('persona-header', PersonaHeader);

