import {html, LitElement} from 'lit-element';
import { render } from 'lit-html';

class PersonaFooter extends LitElement {

    static get properties() {
        return {
            propName: { type: String }
        };
    }
    constructor() {
        super();
        
    }
    render() {
        return html`
            <h5 > @Persona Footer</h5>
            `;

    }
}

customElements.define('persona-footer', PersonaFooter);

