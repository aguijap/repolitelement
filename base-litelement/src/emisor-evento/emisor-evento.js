import { LitElement, html} from 'lit-element';

 

class EmisorEvento extends LitElement {

    render() {

        return html`

            <h3>Emisor EmisorEvento</h3>
            <button @click="${this.sendEvent}">No tocar</button>

        `;

    }
   
    sendEvent(e) {
        console.log("sendEvent");
        console.log("pulsado el boton");

        this.dispatchEvent(
            new CustomEvent(
                "test-event",
                {
                    "detail": {
                        "course": "techun",
                        "year": 2000
                    }
                }
            )
        );
    }
}

 

customElements.define('emisor-evento', EmisorEvento)