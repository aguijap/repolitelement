import { html, LitElement, css } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado'
import '../persona-form/persona-form'

class PersonaMain extends LitElement {

    static get properties() {
        return {
            propName: { type: String },
            people: { type: Array },
            profile: { type: String },
            showPersonForm: {type: Boolean}
        };
    }
    static get styles() {
        return css`
        :host {
            all: initial
        }`
    }
    constructor() {
        super();
        this.showPersonForm = false;
        this.people =
            [{
                name: "Juancho Aguilar",
                yearsInCompany: 12,
                photo: {
                    "src": "./img/img.jpg",
                    "alt": "Bruce Banner"
                },
                profile: "Lorem ipsum dolor sit amet. Lorem ipsum dolor sit ame"
            }
                ,
            {
                name: "Bene Guias",
                yearsInCompany: 24,
                photo: {
                    "src": "./img/img.jpg",
                    "alt": "Bruce Banner"
                },
                profile: "Del mar y los peces Lorem ipsum dolor sit amet."
            },
            {
                name: "Tom Williss",
                yearsInCompany: 17,
                photo: {
                    "src": "./img/img.jpg",
                    "alt": "Bruce Banner"
                },
                profile: "Lorem ipsum dolor sit amet."
            },
            {
                name: "Marcos Del Rio",
                yearsInCompany: 14,
                photo: {
                    "src": "./img/img.jpg",
                    "alt": "Bruce Banner"
                },
                profile: "Lorem ipsum dolor sit amet."
            }
            ]

    }
    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
            <p></p>
            <h2 class="text-center"> Personas </h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map( person => html`<persona-ficha-listado 
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                            fname="${person.name}"
                            yearsInCompany = ${person.yearsInCompany}
                            profile="${person.profile}"
                            .photo="${person.photo}"
                            >
                            </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
                @persona-form-close="${this.personFormClose}"
                @persona-form-store="${this.personFormStore}">
                </persona-form>
            </div>
            `;

    }

    updated(changedProperties) { 
        console.log("updated");	
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }
        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad en persona main");
            this.dispatchEvent(
                new CustomEvent("updated-people", {
                    detail: { people:this.people}
                })
            )
        }
    }
    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	
        }  
    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
        }

    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");

        this.people = this.people.filter(
            person => person.name != e.detail.name
        )
    }
    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");
        //console.log(e.detail.person);
        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);
            this.people = this.people.map(
                person => person.name === e.detail.person.name 
                            ? person = e.detail.person : person
            );
        } else {
            console.log("Se va a almacenar una persona nueva de nombre " + e.detail.person.name);                        
            this.people = [...this.people, e.detail.person];
            console.log("Persona almacenada");
        }
        console.log("Proceso terminado");
        this.showPersonForm = false;
    }
    infoPerson(e){
        console.log("infoPerson en personamain")
        console.log("Se ha pedido info de la persona " + e.detail.name)

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        )

        this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true
      //  console.log(chosenPerson);

        
    }
}

customElements.define('persona-main', PersonaMain);

