import { html, LitElement } from 'lit-element';
import { render } from 'lit-html';

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: { type: String },
            yearsInCompany: { type: String },
            photo: { type: Object },
            personInfo: { type: String}
        };
    }

    constructor() {
        super();
        this.name = "Prueba nombre";
        this.yearsInCompany = 32;
         
        this.photo = {
            src: "https://randomuser.me/api/portraits/men/11.jpg",
            alt: "Foto persona"
        }
        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead"
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior"
        } else if (this.yearsInCompany >=3) {
            this.personInfo = "team"
        } else {
            this.personInfo = "junior"
        }

    }
    updated( changedProperties ) {
        changedProperties.forEach((oldValue, propName) => {
         console.log("Propiedad " + propName + " -----------> cambido valor: " + oldValue )
        })
    }

    render() {
        return html`
            <div>
                <label for="fname">Nombre completo </label>
                <input type="text" id="fname2" name="fname" value="${this.name}" @change="${this.updateName}"></input>
               </br>
               <label for="yearsInCompany">Años en la empresa </label>

                <input type="text" name=yearsInCompany value="${this.yearsInCompany}" @change="${this.updateyearsInCompany}"></input>
    </br>
                <input type="text"  name="personInfo" value=${this.personInfo}></input>
                 
               </br>
               <img src="${this.photo.src}" heigth="100" width="100" alt="Foto persona">
        
            </div>
            `;

    }
    updateName(e){
        console.log("updatename ")
        this.name = e.target.value

    }
    updateyearsInCompany(e){
        this.yearsInCompany = e.target.value
        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead"
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior"
        } else if (this.yearsInCompany >=3) {
            this.personInfo = "team"
        } else {
            this.personInfo = "junior"
        }

    }
}

customElements.define('ficha-persona', FichaPersona);

