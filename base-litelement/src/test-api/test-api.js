import {html, LitElement} from 'lit-element';
import { render } from 'lit-html';

class TestApi extends LitElement {

    static get properties() {
    return {
        movies: { typr: Array}
    }
}
    constructor() {
        super()
        this.movies = []
        this.getMovieData();

    }



    render() {
        return html`
            <div>Test API!</div>
            ${this.movies.map(
                movie =>
                html`<div>La pelicula ${movie.title}, fue dirigida por ${movie.director}. </div>`
            )}
            `;

    }
    getMovieData() {
        console.log("getmoviedata ");
        console.log("Obteniendo datos de la Pelicula")

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente ")

                let APIResponse = JSON.parse(xhr.responseText);

                this.movies = APIResponse.results
                console.log(this.movies)
            }
        }
        xhr.open("GET", "https://swapi.dev/api/films/")
        xhr.send();
    }
}


customElements.define('test-api', TestApi);

